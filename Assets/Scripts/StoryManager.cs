﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StoryManager : MonoBehaviour
{
    [Header("UI Elements")]
    public Image bgImage;
    public Text situationText, storyText;
    public GameObject questionText, nextButton, storyEndButton, replayButton;
    public List<Text> optionTexts;
    public AudioSource audS;
    public Image guyImage, girlImage;
    public ScrollRect scr;
    //public Button optionA, optionB, optionC, optionD;
    [Header("Story Elements")]
    public List<Situations> maleStorySituations;
    public List<Situations> femaleStorySituations;
    [TextArea]
    public string coupleAgain, goodFriends, bitternessPrevailed, fuckUpStory;
    int currentSituationIndex = 0;
    string currentAfterThought;
    List<Situations> sessionSituationList;
    WaitForSeconds waiter = new WaitForSeconds(0.1f);
    int myCurrentKarma = 5, myPartnerCurrentKarma = 5;
    bool breakTypeWriter = false, isTyping, canTakeInput = false, maleProtagonist = false, showingAfterThought = false, showingSituation = false, gameOver = false;
    //bool showingStory = false;
    //Text tempText;
    int storyButtonClickCount = 0;
    public void ShowStory()
    {
        storyButtonClickCount++;
        if (storyButtonClickCount >= 1)
        {
            storyEndButton.SetActive(true);
        }
        scr.content.anchoredPosition = new Vector2(scr.content.anchoredPosition.x, 0);
        storyText.text = fuckUpStory;
    }

    public void StartGame(bool isMale)
    {
        audS.Play();
        if (isMale)
        {
            sessionSituationList = maleStorySituations;
            bgImage.sprite = guyImage.sprite;
        }
        else
        {
            sessionSituationList = femaleStorySituations;
            bgImage.sprite = girlImage.sprite;
        }
        PresentNewSituation();
    }

    /// <summary>
    /// This shows the reaction of the option chosen previosly
    /// </summary>
    /// <param name="premiseText">The text to be shown as reaction</param>
    void SetUpAfterThought(string premiseText)
    {
        ResetUI();
        showingAfterThought = true;
        StartCoroutine(TypeWriter(premiseText));
    }

    /// <summary>
    /// The callback for the option buttons
    /// </summary>
    /// <param name="optionNumber">The index of the option button</param>
    public void SelectedOptionCallback(int optionNumber)
    {
        int index = optionNumber;
        if (sessionSituationList[currentSituationIndex].availableOptions[optionNumber].afterThought.Count > 1)
        {
            index = Random.Range(0, sessionSituationList[currentSituationIndex].availableOptions[optionNumber].afterThought.Count);
            currentAfterThought = sessionSituationList[currentSituationIndex].availableOptions[optionNumber].
                afterThought[index];
        }
        else currentAfterThought = sessionSituationList[currentSituationIndex].availableOptions[optionNumber].
            afterThought[0];
        myCurrentKarma += sessionSituationList[currentSituationIndex].availableOptions[index].selfReward;
        myPartnerCurrentKarma += sessionSituationList[currentSituationIndex].availableOptions[index].partnerReward;

        SetUpAfterThought(currentAfterThought);
        currentSituationIndex = sessionSituationList[currentSituationIndex].availableOptions[optionNumber].nextSituationIndex;
    }

    /// <summary>
    /// Show new Situation
    /// </summary>
    public void PresentNewSituation()
    {
        ResetUI();
        if (currentSituationIndex >= sessionSituationList.Count && !isTyping)
        {
            gameOver = true;
            EvaluateEnding();
            return;
        }
        showingSituation = true;
        StartCoroutine(TypeWriter(sessionSituationList[currentSituationIndex].situation));
    }


    /// <summary>
    /// Typewriter style text presenter, shows text character by character
    /// </summary>
    /// <param name="textToType">The text to be "Typewritten"</param>
    /// <returns>You know that</returns>
    IEnumerator TypeWriter(string textToType)
    {
        int i = 0;
        isTyping = true;
        if (!gameOver)
            canTakeInput = true;
        situationText.text = string.Empty;
        while (!breakTypeWriter && situationText.text.Length < textToType.Length)
        {
            situationText.text += textToType[i];
            i++;
            yield return waiter;
        }

        isTyping = false;
        situationText.text = textToType;
        breakTypeWriter = false;
        canTakeInput = false;

        if (showingAfterThought)
        {
            showingAfterThought = false;
            if (gameOver)
            {
                replayButton.SetActive(true);
                yield break;
            }
            nextButton.SetActive(true);
        }
        else if (showingSituation)
        {
            showingSituation = false;
            PresentTheQuestion();
        }
        //else if(showingStory)
        //{
        //    showingStory = false;
        //    storyEndButton.SetActive(true);
        //    //situationText = tempText;
        //}
    }

    /// <summary>
    /// Game ends here
    /// </summary>
    void EvaluateEnding()
    {
        float val = (float)(myCurrentKarma + myPartnerCurrentKarma) / 2f;
        if (val > 7)
            SetUpAfterThought(coupleAgain);
        else
        if (val > 4)
            SetUpAfterThought(goodFriends);
        else
            SetUpAfterThought(bitternessPrevailed);
    }

    /// <summary>
    /// Show the Question and the options to the player
    /// </summary>
    void PresentTheQuestion()
    {
        int i = 0;
        questionText.SetActive(true);
        //Set text to the option button texts and enable the button gameobjects
        for (; i < sessionSituationList[currentSituationIndex].availableOptions.Count; i++)
        {
            optionTexts[i].text = sessionSituationList[currentSituationIndex].availableOptions[i].option;
            optionTexts[i].transform.parent.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Resets the whole UI for new situation or after-thought
    /// </summary>
    void ResetUI()
    {
        canTakeInput = false;
        situationText.text = "";
        for (int i = 0; i < optionTexts.Count; i++)
            optionTexts[i].transform.parent.gameObject.SetActive(false);
        questionText.SetActive(false);

    }

    public void ReplayGame()
    {
        SceneManager.LoadScene(0);
    }

    private void Update()
    {
        if (gameOver)
            return;
        if (Input.GetMouseButtonDown(0) && canTakeInput)
        {
            canTakeInput = false;
            if (showingSituation)
            {
                breakTypeWriter = true;
            }
            else if (showingAfterThought)
            {
                breakTypeWriter = true;
            }
            //else if(showingStory)
            //{
            //    breakTypeWriter = true;
            //}
        }
    }
}

[System.Serializable]
public struct Situations
{
    [TextArea]
    public string situation;
    public List<Options> availableOptions;
}

[System.Serializable]
public struct Options
{
    [TextArea]
    public string option;
    public List<string> afterThought;
    public int nextSituationIndex;
    public int selfReward, partnerReward;

}
